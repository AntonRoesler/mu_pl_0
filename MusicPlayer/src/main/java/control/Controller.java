package main.java.control;

import java.util.Observable;
import java.util.Observer;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import main.java.model.FileHandling;
import main.java.model.FilePlayer;
import main.java.model.Player;
import main.java.model.TimeSeries;
import main.java.model.VolumeUnitsMeter;
import main.java.view.GUI;

@SuppressWarnings("deprecation")
public class Controller implements Observer {

	private GUI gui;
	private ViewListener viewListener;
	private FileHandling fileHandling;
	private Player player;
	private VolumeUnitsMeter volumeUnitsMeter;
	private Thread volumeUnitsMeterThread;
	private TimeSeries timeSeries;
	private Thread timeSeriesThread;
	
	public Controller() {
		this.gui = new GUI();
		this.fileHandling = new FileHandling();
		this.player = new Player(gui);
		this.viewListener = new ViewListener(this.gui, this.fileHandling, this.player);
		this.registerObserver();
		this.startView();
	}
	
	
	
	@SuppressWarnings("deprecation")
	private void registerObserver() {
		this.fileHandling.addObserver(this);	
	}

	private void startView() {
		if(gui!=null) {
			try {
				UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			SwingUtilities.invokeLater(gui);
		}
	}



	/**
	 * notify when file loaded by FileHandling and generates a new FilePlayer object
	 */

	@Override
	public void update(Observable o, Object arg) {
		if(o instanceof FilePlayer) {
			if(((String)arg).equals("playIsCompleted")) {
				this.gui.getpPanel().getBtnOpen().setEnabled(true);
				this.gui.getpPanel().getBtnPlay().setEnabled(true);
				this.gui.getpPanel().getBtnPause().setEnabled(false);
				this.gui.getpPanel().getBtnStop().setEnabled(false);
				this.player.resetControls();
				this.gui.getPlPanel().getPlottingPanel().reset();
				
				if(this.volumeUnitsMeterThread.isAlive()) {
					this.volumeUnitsMeterThread.interrupt();
				}
				if(this.timeSeriesThread.isAlive()) {
					this.timeSeriesThread.interrupt();
				}
				
				this.volumeUnitsMeterThread = new Thread(this.volumeUnitsMeter);
				this.volumeUnitsMeterThread.start();
				
				this.timeSeriesThread = new Thread(this.timeSeries);
				this.timeSeriesThread.start();
			}
			
			if(((String)arg).equals("loadedAudioBuffer")) {
				this.volumeUnitsMeter = new VolumeUnitsMeter(this.player.getFilePlay(), gui.getpPanel().getUvPanel(), this.player);
				this.timeSeries = new TimeSeries(this.player.getFilePlay(), gui.getPlPanel().getPlottingPanel(), this.player);	
				
				this.volumeUnitsMeterThread = new Thread(this.volumeUnitsMeter);
				this.volumeUnitsMeterThread.start();
				
				this.timeSeriesThread = new Thread(this.timeSeries);
				this.timeSeriesThread.start();
			}
		}
		
		if(o instanceof FileHandling) {
			this.getPlayer().setFilePlay(new FilePlayer((String)arg));
			this.gui.getpPanel().getBtnPlay().setEnabled(true);
			this.player.getFilePlay().addObserver(this);
		}
	}



	public Player getPlayer() {
		return player;
	}



	public void setPlayer(Player player) {
		this.player = player;
	}



	public VolumeUnitsMeter getVolumeUnitsMeter() {
		return volumeUnitsMeter;
	}



	public void setVolumeUnitsMeter(VolumeUnitsMeter volumeUnitsMeter) {
		this.volumeUnitsMeter = volumeUnitsMeter;
	}

}
