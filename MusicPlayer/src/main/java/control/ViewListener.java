package main.java.control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JButton;
import javax.swing.text.BadLocationException;

import main.java.model.FileHandling;
import main.java.model.Player;
import main.java.view.GUI;

@SuppressWarnings("deprecation")
public class ViewListener implements ActionListener, Observer {

	private GUI gui;
	private FileHandling fileHandling;
	private Player player;
	
	public ViewListener(GUI gui, FileHandling fileHandling, Player player){
		this.gui = gui;
		this.fileHandling = fileHandling;
		this.player = player;
		if(this.gui!=null) {
			 register(); 
			 registerObserver();
		}
	}
	
	private void register() {
		gui.getpPanel().getBtnOpen().addActionListener(this);
		gui.getpPanel().getBtnPlay().addActionListener(this);
		gui.getpPanel().getBtnPause().addActionListener(this);
		gui.getpPanel().getBtnStop().addActionListener(this);
		gui.getpPanel().getBtnRecord().addActionListener(this);
	}
	private void registerObserver() {
		this.player.addObserver(this);	
	}
	@Override
	public void actionPerformed(ActionEvent event) {
		Object source = event.getSource();
		if (source instanceof JButton) {
			JButton button = (JButton) source;
			if (button == gui.getpPanel().getBtnOpen()) {
				this.fileHandling.openFile(gui);
				try {
					this.player.loadFilePlayer();
				} catch (UnsupportedAudioFileException usafe) {
					usafe.printStackTrace();
				} catch (IOException ioe) {
					//ioe.printStackTrace();
				} catch (LineUnavailableException lue) {
					lue.printStackTrace();
				}
			} else if (button == gui.getpPanel().getBtnPlay()) {
				if (!player.isPlaying()) {
					player.playBack();
				} else {
					player.stopPlaying();
				}
			} else if (button == gui.getpPanel().getBtnPause()) {
				if (!player.isPause()) {
					player.pausePlaying();
				} else {
					player.resumePlaying();
				}
			} else if (button == gui.getpPanel().getBtnStop()) {
				if (!player.isPause()) {
					player.stopPlaying();
				} else {
					player.resumePlaying();
				}
			}
		}
	}

	@Override
	public void update(Observable o, Object arg) {	
		if(o instanceof Player) {
			if(((String)arg).equals("isPlay")) {
				gui.getpPanel().getBtnOpen().setEnabled(false);
				gui.getpPanel().getBtnPlay().setEnabled(false);
				gui.getpPanel().getBtnPause().setEnabled(true);
				gui.getpPanel().getBtnStop().setEnabled(true);
			} else if(((String)arg).equals("isReset")) {
				gui.getpPanel().getBtnOpen().setEnabled(true);
				gui.getpPanel().getBtnPlay().setEnabled(true);
				gui.getpPanel().getBtnPause().setEnabled(false);
				gui.getpPanel().getBtnStop().setEnabled(false);
			} else if(((String)arg).equals("isPause")) {
				gui.getpPanel().getBtnStop().setEnabled(false);
			} else if(((String)arg).equals("isResume")) {
				gui.getpPanel().getBtnStop().setEnabled(true);
			} else if(((String)arg).contains("duration")) {
				gui.getpPanel().getLabelDuration().setText(((String)arg).split(",")[1]);
			} else if(((String)arg).contains("loadedFile")) {
				try {
					gui.getdPanel().appendPane(player.queryDataInfo());
				} catch (BadLocationException e) {
					e.printStackTrace();
				}
				
			}
		}
		
	}
	
}
