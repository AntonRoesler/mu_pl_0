package main.java.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.text.AttributeSet;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;

@SuppressWarnings("serial")
public class DataPanel extends JPanel {

	private JTextPane pane;  
	private Document doc;
	private SimpleAttributeSet attributeSet;  

	private JScrollPane scrollPane;

	public DataPanel(Dimension d, Color bgc) {
		init(d, bgc);
	}

	private void init(Dimension d, Color bgc) {
		setPane(new JTextPane());
		getPane().setEditable(false);
		setDoc(pane.getStyledDocument());  
		setAttributeSet(new SimpleAttributeSet());
		setStyleConstants();  
        
        
		pane.setCharacterAttributes(getAttributeSet(), true);  
		
		pane.setText("Load a file!");  
		
		setScrollPane(new JScrollPane(getPane()));
		getScrollPane().setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		getScrollPane().setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		//this.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		this.add(getScrollPane(), BorderLayout.CENTER);
		this.setBackground(bgc);
		getPane().setPreferredSize(d);
		getPane().setBackground(Color.black);
	}

	private void setStyleConstants() {
		StyleConstants.setBold(attributeSet, true);    
        StyleConstants.setForeground(attributeSet, Color.yellow);  
        StyleConstants.setBackground(attributeSet, Color.black);
	}
	
	public void appendPane(Map<String, String> append) throws BadLocationException {
		pane.setText("");//clear
		SimpleAttributeSet attributeSet = new SimpleAttributeSet();
		StyleConstants.setForeground(attributeSet, Color.yellow);
		StyleConstants.setBackground(attributeSet, Color.black);
		pane.setCharacterAttributes(attributeSet, true);
		for (Map.Entry<String,String> entry : append.entrySet()) {
			StyleConstants.setBold(attributeSet, true); 
			StyleConstants.setItalic(attributeSet, false);
			doc.insertString(doc.getLength(), entry.getKey()+" ", attributeSet);
			StyleConstants.setBold(attributeSet, false); 
			StyleConstants.setItalic(attributeSet, true);
			doc.insertString(doc.getLength(), entry.getValue()+"\n", attributeSet);
		}
	}
	
	public void showSize() {
		System.out.println(getPane().getSize().height+"  "+getPane().getSize().width);
	}
	
	public Document getDoc() {
		return doc;
	}

	public void setDoc(Document doc) {
		this.doc = doc;
	}

	public JScrollPane getScrollPane() {
		return scrollPane;
	}
	public void setScrollPane(JScrollPane scrollPane) {
		this.scrollPane = scrollPane;
	}

	public JTextPane getPane() {
		return pane;
	}

	public void setPane(JTextPane pane) {
		this.pane = pane;
	}

	public SimpleAttributeSet getAttributeSet() {
		return attributeSet;
	}

	public void setAttributeSet(SimpleAttributeSet attributeSet) {
		this.attributeSet = attributeSet;
	}
	
	
	
	
}
