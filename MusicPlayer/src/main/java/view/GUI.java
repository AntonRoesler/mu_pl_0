package main.java.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Point;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;

@SuppressWarnings("serial")
public class GUI extends JFrame implements Runnable {

	private int frameHeight;
	private int frameWidth;
	private Dimension frameSize;
	private Dimension pPanelSize;
	private Dimension dPanelSize;
	private Dimension plPanelSize;
	private Properties props;
	private PlayerPanel pPanel;
	private DataPanel dPanel;
	private PlottingPanel plPanel;
	private Color backgroundColor;
	
	public GUI() {
		super("Music Player");
		this.configGUI();
		this.initFrame();
	}
	
	
	@Override
	public void run() {
		this.setVisible(true);
	}
	
	private void initFrame() {
		
		this.setFrameSize(frameSize);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);
		
		try {
			Image image = ImageIO.read(GUI.class.getResource("../../../main/resources/icons/music_note.png"));
			setIconImage(image);
		} catch (IOException e) {
			e.printStackTrace();
		}
		

		//this.setIconImage(new ImageIcon(getClass().getResource("src/main/resources/icons/music_note.svg")).getImage());
		this.setpPanel(new PlayerPanel(getpPanelSize(), getBackgroundColor()));
		this.setdPanel(new DataPanel(getdPanelSize(), getBackgroundColor()));
		this.setPlPanel(new PlottingPanel(getdPanelSize(), getBackgroundColor()));
		
		GridBagLayout layout = new GridBagLayout();
		Utility.addComponent(this, layout, this.getpPanel(), 0, 0, 1, 1, 1, 1);
		Utility.addComponent(this, layout, this.getdPanel(), 0, 1, 1, 1, 1, 1);
		Utility.addComponent(this, layout, this.getPlPanel(), 0, 2, 1, 1, 0.5, 0.5);
		this.setLayout(layout);
		
		this.pack();
		this.setLocationRelativeTo(null);  // center
		
	}

	private void configGUI() {
		this.props = new Properties();
		String w, h;
			try(FileReader fr = new FileReader("src/main/assets/config.ini")){
				this.props.load(fr);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		if(this.props.containsKey("frame_width") && this.props.containsKey("frame_height")) {
			w = (String) this.props.get("frame_width");
			h = (String) this.props.get("frame_height");
			setFrameSize(new Dimension(Integer.parseInt(w) , Integer.parseInt(h)));   
		}
		if(this.props.containsKey("pPanel_width") && this.props.containsKey("pPanel_height")) {
			w = (String) this.props.get("pPanel_width");
			h = (String) this.props.get("pPanel_height");
			setpPanelSize(new Dimension(Integer.parseInt(w) , Integer.parseInt(h)));   
		}
		if(this.props.containsKey("dPanel_width") && this.props.containsKey("dPanel_height")) {
			w = (String) this.props.get("dPanel_width");
			h = (String) this.props.get("dPanel_height");
			setdPanelSize(new Dimension(Integer.parseInt(w) , Integer.parseInt(h)));   
		}
		
		if(this.props.containsKey("bgColor")) {
			String[] bgc = ((String) this.props.get("bgColor")).split(",");
			setBackgroundColor(new Color(Integer.parseInt(bgc[0].trim()), Integer.parseInt(bgc[1].trim()), Integer.parseInt(bgc[2].trim())));
			
		}
		if(this.props.containsKey("plPanel_width") && this.props.containsKey("plPanel_height")) {
			w = (String) this.props.get("plPanel_width");
			h = (String) this.props.get("plPanel_height");
			setPlPanelSize(new Dimension(Integer.parseInt(w) , Integer.parseInt(h)));   
		}
		
	}
	
	/**
	 * 
	 * setter and getter
	 */
	public void setFrameSize(Dimension size) {
		this.setPreferredSize(size);
	}

	public Dimension getFrameSize() {
		return this.getPreferredSize();
	}
	
	public void setFramePos(int x, int y) {
		this.setLocation(x, y);;
	}

	public Point getFramePos() {
		return this.getLocation();
	}
	

	public PlayerPanel getpPanel() {
		return pPanel;
	}


	public void setpPanel(PlayerPanel pPanel) {
		this.pPanel = pPanel;
	}


	public DataPanel getdPanel() {
		return dPanel;
	}


	public void setdPanel(DataPanel dPanel) {
		this.dPanel = dPanel;
	}


	public PlottingPanel getPlPanel() {
		return plPanel;
	}


	public void setPlPanel(PlottingPanel plPanel) {
		this.plPanel = plPanel;
	}


	public int getFrameHeight() {
		return frameHeight;
	}


	public void setFrameHeight(int frameHeight) {
		this.frameHeight = frameHeight;
	}


	public int getFrameWidth() {
		return frameWidth;
	}


	public void setFrameWidth(int frameWidth) {
		this.frameWidth = frameWidth;
	}


	public Dimension getpPanelSize() {
		return pPanelSize;
	}


	public void setpPanelSize(Dimension pPanelSize) {
		this.pPanelSize = pPanelSize;
	}


	public Dimension getdPanelSize() {
		return dPanelSize;
	}


	public void setdPanelSize(Dimension dPanelSize) {
		this.dPanelSize = dPanelSize;
	}


	public Dimension getPlPanelSize() {
		return plPanelSize;
	}


	public void setPlPanelSize(Dimension plPanelSize) {
		this.plPanelSize = plPanelSize;
	}


	public Color getBackgroundColor() {
		return backgroundColor;
	}


	public void setBackgroundColor(Color backgroundColor) {
		this.backgroundColor = backgroundColor;
	}
	
	
	
}
