package main.java.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class PlayerPanel extends JPanel {
	
	private JLabel labelTimeTitle;
	private JLabel labelTime;		//timer
	private JLabel labelDuration;	//clip duration
	
	private JButton btnOpen;
	private JButton btnPlay;
	private JButton btnPause;
	private JButton btnStop;
	private JButton btnRecord;
	private GraphPanel uvPanel;
	
	public PlayerPanel(Dimension d, Color bgc) {
		init(d, bgc);
	}
	
	private void init(Dimension d, Color bgc) {
		this.setBackground(bgc);
		this.setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.insets = new Insets(5, 5, 5, 5);
		constraints.anchor = GridBagConstraints.WEST;
		
		this.setLabelTimeTitle(new JLabel("Time:"));
		this.setLabelTime(new JLabel("00:00:00"));
		this.setLabelDuration(new JLabel("00:00:00"));
		
		this.setBtnOpen(new JButton("Open"));
		this.setBtnPlay(new JButton("Play"));
		this.setBtnPause(new JButton("Pause"));
		this.setBtnStop(new JButton("Stop"));
		this.setBtnRecord(new JButton("Record"));
		
		int btnWidth = 10;
		int btnheight = 10;
		
		this.getBtnOpen().setText("");
		ImageIcon icon = new ImageIcon(PlayerPanel.class.getResource("../../../main/resources/icons/open.png"));
		this.getBtnOpen().setIcon(Utility.resizeIcon(icon, btnWidth, btnheight));
		
		this.getBtnPlay().setText("");
		icon = new ImageIcon(PlayerPanel.class.getResource("../../../main/resources/icons/play.png"));
		this.getBtnPlay().setIcon(Utility.resizeIcon(icon, btnWidth, btnheight));
		this.getBtnPlay().setEnabled(false);
		
		this.getBtnPause().setText("");
		icon = new ImageIcon(PlayerPanel.class.getResource("../../../main/resources/icons/pause.png"));
		this.getBtnPause().setIcon(Utility.resizeIcon(icon, btnWidth, btnheight));
		this.getBtnPause().setEnabled(false);
		
		this.getBtnStop().setText("");
		icon = new ImageIcon(PlayerPanel.class.getResource("../../../main/resources/icons/stop.png"));
		this.getBtnStop().setIcon(Utility.resizeIcon(icon, btnWidth, btnheight));
		this.getBtnStop().setEnabled(false);
		
		this.getBtnRecord().setText("");
		icon = new ImageIcon(PlayerPanel.class.getResource("../../../main/resources/icons/record.png"));
		this.getBtnRecord().setIcon(Utility.resizeIcon(icon, btnWidth, btnheight));
		this.getBtnRecord().setEnabled(false);
		
		
		this.getLabelTimeTitle().setFont(new Font("Sans", Font.BOLD, 12));
		this.getLabelTimeTitle().setForeground(Color.orange);
		
		this.getLabelTime().setFont(new Font("Sans", Font.BOLD, 12));
		this.getLabelTime().setForeground(Color.orange);
		
		this.getLabelDuration().setFont(new Font("Sans", Font.BOLD, 12));
		this.getLabelDuration().setForeground(Color.orange);

		this.setUvPanel(new GraphPanel("vu"));
		

		GridBagLayout layout = new GridBagLayout();
		Utility.addComponent(this, layout, this.getLabelTimeTitle(), 0, 0, 1, 1, 0.5, 0.5);
		Utility.addComponent(this, layout, this.getLabelTime(),      1, 0, 1, 1, 0.5, 0.5);
		Utility.addComponent(this, layout, this.getLabelDuration(),  2, 0, 1, 1, 0.5, 0.5);
		Utility.addComponent(this, layout, this.getBtnOpen(),        0, 1, 1, 1, 0.5, 0.5);
		Utility.addComponent(this, layout, this.getBtnPlay(),        1, 1, 1, 1, 0.5, 0.5);
		Utility.addComponent(this, layout, this.getBtnPause(),       2, 1, 1, 1, 0.5, 0.5);
		Utility.addComponent(this, layout, this.getBtnStop(),        3, 1, 1, 1, 0.5, 0.5);
//		Utility.addComponent(this, layout, this.getBtnRecord(),      4, 1, 1, 1, 0.5, 0.5);
		Utility.addComponent(this, layout, this.getUvPanel(),        5, 0, 2, 2, 1, 1);
		this.setLayout(layout);
		
	}

	public JLabel getLabelTimeTitle() {
		return labelTimeTitle;
	}

	public void setLabelTimeTitle(JLabel labelTimeTitle) {
		this.labelTimeTitle = labelTimeTitle;
	}

	public JLabel getLabelTime() {
		return labelTime;
	}

	public void setLabelTime(JLabel labelTime) {
		this.labelTime = labelTime;
	}

	public JLabel getLabelDuration() {
		return labelDuration;
	}

	public void setLabelDuration(JLabel labelDuration) {
		this.labelDuration = labelDuration;
	}

	public JButton getBtnOpen() {
		return btnOpen;
	}

	public void setBtnOpen(JButton btnOpen) {
		this.btnOpen = btnOpen;
	}

	public JButton getBtnPlay() {
		return btnPlay;
	}

	public void setBtnPlay(JButton btnPlay) {
		this.btnPlay = btnPlay;
	}

	public JButton getBtnPause() {
		return btnPause;
	}

	public void setBtnPause(JButton btnPause) {
		this.btnPause = btnPause;
	}

	public JButton getBtnStop() {
		return btnStop;
	}

	public void setBtnStop(JButton btnStop) {
		this.btnStop = btnStop;
	}

	public JButton getBtnRecord() {
		return btnRecord;
	}

	public void setBtnRecord(JButton btnRecord) {
		this.btnRecord = btnRecord;
	}

	public GraphPanel getUvPanel() {
		return uvPanel;
	}

	public void setUvPanel(GraphPanel uvPanel) {
		this.uvPanel = uvPanel;
	}
}
