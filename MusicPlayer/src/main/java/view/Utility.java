package main.java.view;

import java.awt.Component;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;

import javax.swing.Icon;
import javax.swing.ImageIcon;

public class Utility {

	/**
	 * Helper function for creating a GridBagConstraints object.
	 * Set constraints for both x an y dimensions.
	 * 
	 * @param container
	 * @param gridBagLayout
	 * @param component
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @param weightx
	 * @param weighty
	 */
	static void addComponent(Container container, GridBagLayout gridBagLayout, Component component, int x, int y, int width, int height,
			double weightx, double weighty) {

		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.BOTH;
		gbc.insets = new Insets(5,5, 5, 5);
		gbc.gridx = x;
		gbc.gridy = y;
		gbc.gridwidth = width;
		gbc.gridheight = height;
		gbc.weightx = weightx;
		gbc.weighty = weighty;
		gridBagLayout.setConstraints(component, gbc);
		container.add(component);
	}
	
	/**
	 * Resizing the size of a loaded icon to fit the button size.
	 * 
	 * @param icon
	 * @param resizedWidth
	 * @param resizedHeight
	 * @return
	 */
	static Icon resizeIcon(ImageIcon icon, int resizedWidth, int resizedHeight) {
	    Image img = icon.getImage();  
	    Image resizedImage = img.getScaledInstance(resizedWidth, resizedHeight,  java.awt.Image.SCALE_SMOOTH);  
	    return new ImageIcon(resizedImage);
	}
	 
}
