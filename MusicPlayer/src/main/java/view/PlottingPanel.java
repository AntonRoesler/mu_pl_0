package main.java.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class PlottingPanel extends JPanel {
	private GraphPanel plottingPanel;
	public PlottingPanel(Dimension d, Color bgc) {
		init(d, bgc);
	}

	private void init(Dimension d, Color bgc) {
		this.setBackground(bgc);
		this.setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.insets = new Insets(5, 5, 5, 5);
		constraints.anchor = GridBagConstraints.WEST;
		
		this.setPlottingPanel(new GraphPanel("oscilloscope"));
		this.getPlottingPanel().setPreferredSize(d);
		GridBagLayout layout = new GridBagLayout();
		Utility.addComponent(this, layout, this.getPlottingPanel(),        0, 0, 1, 1, 1, 1);
		this.setLayout(layout);
		
	}

	public GraphPanel getPlottingPanel() {
		return plottingPanel;
	}

	public void setPlottingPanel(GraphPanel plottingPanel) {
		this.plottingPanel = plottingPanel;
	}
	
}
