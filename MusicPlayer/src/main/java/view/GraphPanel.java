package main.java.view;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Consumer;

import javax.swing.JPanel;


@SuppressWarnings("serial")
public class GraphPanel extends JPanel {
	private String type;
	
	//attributes for the "volume units meter"
	private final int ARC_RECT = 5;
	private double vuLeft = 0;
	private double vuRight = 0;

	//attributes for the "oscilloscope"
	private int panelPadding = 15;//space to all four JPanel borders
	private Color lineColor = new Color(210, 254, 222, 180);
	private Color gridColor = new Color(200, 200, 200, 200);
	private static final Stroke GRAPH_STROKE = new BasicStroke(2f);//line width
	private int numberYDivisions = 5;//Y-grid lines
	private int numberXDivisions = 10;//X-grid lines
	private Queue<Double> scores;

	private Consumer<Graphics2D> draw;
	public GraphPanel(String type){
		this.type = type;
		if(type.equals("vu")) {
			this.setBackground(Color.BLACK);
			draw = g1 -> drawVUmeter(g1);
		} else if(type.equals("oscilloscope")) {
			this.setBackground(new Color(4, 195, 194, 255));
			draw = g1 -> drawOscilloscope(g1);
		}
	}

	@Override
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		Graphics2D g1= (Graphics2D) g;
		g1.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
		draw.accept(g1);
	}

	private void drawOscilloscope(Graphics2D g1) {
			drawLine(g1);
			drawGrid(g1);
	}

	private void drawLine(Graphics2D g1) {
		if(this.scores != null) {
			List<Point> graphPoints = new ArrayList<>();

			//fitting data points within JPanel size -->left&right-"padding" and additionally one sited-"labelPadding":
			//step sizes
			double delta_xScale = ((double) getWidth() - (2 * panelPadding)) / (scores.size() - 1);
			double delta_yScale = ((double) getHeight() - (2 * panelPadding) );

			synchronized(this.scores) {
			int count = 0;
			for (int i = 0; i < scores.size(); i++, count++) {
				graphPoints.add(new Point((int) (count * delta_xScale + panelPadding), (int) (scores.poll() * delta_yScale + panelPadding)));
				i--;
			}
			}

			//---plot the graph line--
			Stroke oldStroke = g1.getStroke();
			g1.setColor(lineColor);
			g1.setStroke(GRAPH_STROKE);
			for (int i = 0; i < graphPoints.size() - 1; i++) {
				int x1 = graphPoints.get(i).x;
				int y1 = graphPoints.get(i).y;
				int x2 = graphPoints.get(i + 1).x;
				int y2 = graphPoints.get(i + 1).y;
				g1.drawLine(x1, y1, x2, y2);
			}
			g1.setStroke(oldStroke);
		}
	}

	private void drawGrid(Graphics2D g1) {
		double delta_x = ((double) getWidth() - (2 * panelPadding)) / (numberXDivisions - 1);
		double delta_y = ((double) getHeight() - (2 * panelPadding))/ (numberYDivisions - 1);
		for(int i = 0; i<numberXDivisions;i++) {
			drawLineTypes(g1, (int)(panelPadding +delta_x*i), panelPadding, (int)(panelPadding +delta_x*i), getHeight()-panelPadding, 0);
		}
		for(int i = 0; i<numberYDivisions;i++) {
			drawLineTypes(g1, panelPadding, (int)(panelPadding +delta_y*i), getWidth()-panelPadding, (int)(panelPadding +delta_y*i), 0);
		}
		
	}
	
	public static void drawLineTypes(Graphics2D g1, int x0, int y0, int x1, int y1, int LineType) {
		float[] dash1 = { 2f, 0f, 2f };
		float[] dash2 = { 1f, 1f, 1f };
		float[] dash3 = { 4f, 0f, 2f };
		float[] dash4 = { 4f, 4f, 1f };

		Stroke oldStroke = g1.getStroke();
		BasicStroke bs1;
		switch (LineType) {
		case 0:
			bs1 = new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND, 1.0f, dash1, 2f);
			break;
		case 1:
			bs1 = new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND, 1.0f, dash2, 2f);
			break;

		case 2:
			bs1 = new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND, 1.0f, dash3, 2f);
			break;
		case 3:
			bs1 = new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND, 1.0f, dash4, 2f);
			break;
		default:
			bs1 = new BasicStroke(1f);
			break;
		}
		g1.setStroke(bs1);
		g1.setColor(Color.BLACK);
		g1.drawLine(x0, y0, x1, y1);
		g1.setStroke(oldStroke);
	}
	
	private void drawVUmeter(Graphics2D g1) {
		int barWidth = (getWidth()/2-10);
		int maxLevel = 5;
		int zeroLevel = getHeight() - maxLevel;
		int maxValue = zeroLevel - maxLevel;

		double log2 = Math.log(2);
		double log2max = (Math.log(Short.MAX_VALUE)/log2);
		double log2min = - 5;

		double log2_vuLeft = (this.vuLeft>0)?Math.log(this.vuLeft)/log2:0;
		double log2_vuRight = (this.vuRight>0)?Math.log(this.vuRight)/log2:0;
		log2_vuLeft /=log2max;
		log2_vuRight /=log2max;

		int levelLeft = (int) (log2_vuLeft * maxValue);
		int posLeft = zeroLevel-levelLeft;

		int levelRight = (int) (log2_vuRight * maxValue);
		int posRight = zeroLevel-levelRight;

		g1.setPaint(Color.ORANGE);
		g1.fillRoundRect(10 , posLeft, barWidth, levelLeft, ARC_RECT, ARC_RECT);
		g1.setPaint(Color.ORANGE);
		g1.fillRoundRect(8 + (getWidth()/2-5), posRight, barWidth, levelRight, ARC_RECT, ARC_RECT);
	}

	@Override
	public void repaint() {
		synchronized(GraphPanel.class) {
			super.repaint();
		}
	}

	public void setVU(double vUleft2, double vUright2) {
		synchronized(GraphPanel.class) {
			this.vuLeft = vUleft2;
			this.vuRight = vUright2;
		}
	}

	public double getVuLeft() {
		return vuLeft;
	}

	public void setVuLeft(int vuLeft) {
		this.vuLeft = vuLeft;
	}

	public double getVuRight() {
		return vuRight;
	}

	public void setVuRight(int vuRight) {
		this.vuRight = vuRight;
	}

	private double windowingHamming(double n, double periode) {
		return 0.54 -0.46 * Math.cos(2*Math.PI*n/(periode-1));
	}

	public Queue<Double> getScores() {
		return scores;
	}

	public void setScores(Queue<Double> scores) {
		synchronized(GraphPanel.class) {
			this.scores = scores;
		}
	}
	
	public void reset() {
		this.removeAll();
		if(type.equals("vu")) {
			this.setBackground(Color.BLACK);
			draw = g1 -> drawVUmeter(g1);
		} else if(type.equals("oscilloscope")) {
			this.setBackground(new Color(4, 195, 194, 255));
			draw = g1 -> drawOscilloscope(g1);
		}
		this.repaint();
	}
	
	private void generateTestScores(int size) {
		this.scores = new LinkedList<Double>();
		for(int i = 0; i < size; i++) {
			this.scores.add(ThreadLocalRandom.current().nextDouble(-1, 1));
		}
	}

}
