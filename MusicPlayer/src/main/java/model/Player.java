package main.java.model;

import java.io.IOException;
import java.util.Map;
import java.util.Observable;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import main.java.view.GUI;

@SuppressWarnings("deprecation")
public class Player extends Observable {
	private FilePlayer filePlay; //generates an audio stream from a given file path
	private PlayTimerWorker timer;
	private Runnable playbackTask = ()->{
		setPlayNotify();
		try {
			runPlayer();
		} catch (IOException ex) {
			resetControls();
			ex.printStackTrace();
		} catch (LineUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	};
	Thread playbackWorker;
	private boolean isPlaying;
	private boolean isPause;
	private GUI gui;
	
	public Player(GUI gui) {
		this.setGui(gui);
	}
	
	private void setPlayNotify() {
		setChanged();
		notifyObservers("isPlay");	
	}
	private void setResetNotify() {
		setChanged();
		notifyObservers("isReset");	
	}
	
	
	public void playBack() {
		if(this.getFilePlay()==null) return;
		//must be better resolved with respect to the MVC structure
		this.setTimer(new PlayTimerWorker(this.getGui().getpPanel().getLabelTime()));
		this.getTimer().start();
		this.setPlaying(true);
		
		playbackWorker = new Thread(playbackTask);
		playbackWorker.start();
	}

	private void runPlayer() throws IOException, LineUnavailableException {
			getTimer().setAudioClip(getFilePlay().getAudioClip());
			setChanged();
			notifyObservers("duration,"+getFilePlay().getClipLengthFormatedString());
			getFilePlay().play();
	}
	
	public Map<String,String> queryDataInfo() {
		return this.getFilePlay().getInfo();
	}
	
	public void loadFilePlayer() throws UnsupportedAudioFileException, IOException, LineUnavailableException {
		this.getFilePlay().load();
		setChanged();
		notifyObservers("loadedFile");
	}
	
	public void stopPlaying() {
		if(this.getFilePlay()==null) return;
		this.setPause(false);
		this.getTimer().reset();
		this.getTimer().interrupt();
		this.getFilePlay().stop();
		playbackWorker.interrupt();
		setResetNotify();
		resetControls();
	}

	public void pausePlaying() {
		if(this.getFilePlay()==null) return;
		this.setPause(true);
		this.getTimer().pauseTimer();
		this.getFilePlay().pause();
		playbackWorker.interrupt();
		setChanged();
		notifyObservers("isPause");	//notify ViewListener
	}

	public void resumePlaying() {
		if(this.getFilePlay()==null) return;
		this.setPause(false);
		this.getTimer().resumeTimer();
		this.getFilePlay().resume();
		playbackWorker.interrupt();	
		setChanged();
		notifyObservers("isResume"); //notify to ViewListener	
	}

	public void resetControls() {
		if(this.getFilePlay()==null) return;
		this.setPlaying(false);
		this.getTimer().reset();
		this.getTimer().interrupt();
		this.getFilePlay().resetFlags();
	}
	
	//setter and getter
	public PlayTimerWorker getTimer() {
		return timer;
	}

	public void setTimer(PlayTimerWorker timer) {
		this.timer = timer;
	}

	public Runnable getPlaybackTask() {
		return playbackTask;
	}

	public void setPlaybackTask(Runnable playbackTask) {
		this.playbackTask = playbackTask;
	}

	public boolean isPlaying() {
		return isPlaying;
	}

	public void setPlaying(boolean isPlaying) {
		this.isPlaying = isPlaying;
	}

	public boolean isPause() {
		return isPause;
	}

	public void setPause(boolean isPause) {
		this.isPause = isPause;
	}

	public GUI getGui() {
		return gui;
	}

	public void setGui(GUI gui) {
		this.gui = gui;
	}

	public FilePlayer getFilePlay() {
		return filePlay;
	}

	public void setFilePlay(FilePlayer filePlay) {
		//System.out.println("FilePlay in Player set!");
		this.filePlay = filePlay;
	}

}
