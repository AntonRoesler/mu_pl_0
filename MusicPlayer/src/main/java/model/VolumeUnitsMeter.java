package main.java.model;

import java.util.function.DoubleBinaryOperator;
import main.java.view.GraphPanel;

public class VolumeUnitsMeter implements Runnable {

	private long updateTimeMillis = 10;
	private double overflowThresh = 1.; 
	private double vUleft;
	private double vUright;
	private double bitDepth;
	private FilePlayer filePlayer;
	private GraphPanel vuPanel;
	private Player player;

	public VolumeUnitsMeter(FilePlayer filePlayer, GraphPanel vuPanel, Player player){
		this.filePlayer = filePlayer;
		this.vuPanel = vuPanel;
		this.player = player;
		this.bitDepth = Math.pow(2, this.filePlayer.getBitDepth());
		this.overflowThresh *= this.bitDepth;
	}

	@Override
	public void run() {
		int position = 0;
		double amplitudeLeft = 0;
		double amplitudeRight = 0;
		this.vUleft = 0.0;
		this.vUright = 0.0;
		DoubleBinaryOperator ibo = (prev, next)-> (next!=0 || prev!=0) ? prev + next - (0.8*prev):0; 


		if(this.filePlayer.getBuffer().length == 1) { //mono
			while(!this.filePlayer.isStopped()) {
				try {
					Thread.sleep(updateTimeMillis);
				} catch (InterruptedException e) {
//					e.printStackTrace();
					break;
				}
				synchronized (FilePlayer.class) {
					if(this.player.getTimer()!=null)
						position = (int) this.player.getTimer().getTimeMillisec()*this.player.getFilePlay().getSamplingRate()/1000;
					if(position >=0 && position < this.filePlayer.getBuffer()[0].length) { 
						amplitudeLeft = Math.abs(this.filePlayer.getBuffer()[0][position])*this.bitDepth;
					}
					this.vUleft = ibo.applyAsDouble(this.vUleft, Math.abs(amplitudeLeft));
					this.vUright = this.vUleft;

					if( this.vUleft > overflowThresh) {
						this.vUleft = overflowThresh;
						this.vUright = overflowThresh;
					}
				}

				synchronized(GraphPanel.class) {
					if(!this.filePlayer.isPaused() && this.filePlayer.isPlaying()) {
						this.vuPanel.setVU(this.vUleft, this.vUright);
						this.vuPanel.repaint();
					} else if(this.filePlayer.isStopped() || this.filePlayer.isPlayCompleted()){
						this.vuPanel.setVU(0, 0);
						this.vuPanel.repaint();
					}
				}

			}
			synchronized(GraphPanel.class) {
				this.vuPanel.setVU(0, 0);
				this.vuPanel.repaint();
			}
		} else if(this.filePlayer.getBuffer().length == 2) {//stereo
			while(!this.filePlayer.isStopped()) {
				try {
					Thread.sleep(updateTimeMillis);
				} catch (InterruptedException e) {
//					e.printStackTrace();
					break;
				}
				synchronized (FilePlayer.class) {
					if(this.player.getTimer()!=null)
						position = (int) this.player.getTimer().getTimeMillisec()*this.player.getFilePlay().getSamplingRate()/1000;
					if(position >=0 && position<this.filePlayer.getBuffer()[0].length) {
						amplitudeLeft = Math.abs(this.filePlayer.getBuffer()[0][position])*this.bitDepth;
						amplitudeRight = Math.abs(this.filePlayer.getBuffer()[1][position])*this.bitDepth;

					}
					this.vUleft = ibo.applyAsDouble(this.vUleft, Math.abs(amplitudeLeft));
					this.vUright = ibo.applyAsDouble(this.vUleft, Math.abs(amplitudeRight));
					if( this.vUleft > overflowThresh) {
						this.vUleft = overflowThresh;
					}
					if( this.vUright > overflowThresh) {
						this.vUright = overflowThresh;
					}

				}

				synchronized(GraphPanel.class) {
					if(!this.filePlayer.isPaused() && this.filePlayer.isPlaying()) {
						this.vuPanel.setVU(this.vUleft, this.vUright);
						this.vuPanel.repaint();
					} else if(this.filePlayer.isStopped() || this.filePlayer.isPlayCompleted()){
						this.vuPanel.setVU(0, 0);
						this.vuPanel.repaint();
					}
				}

			}
			synchronized(GraphPanel.class) {
				this.vuPanel.setVU(0, 0);
				this.vuPanel.repaint();
			}
		}
	}
}
