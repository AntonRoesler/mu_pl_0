package main.java.model;

import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Observable;
import java.util.TreeMap;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import lib.wavfile.WavFile;

@SuppressWarnings("deprecation")
public class FilePlayer extends Observable implements LineListener {
	/**
	 * for calculating hours and minutes from given seconds
	 */
	private static final int SECONDS_IN_HOUR = 3_600;
	private static final int SECONDS_IN_MINUTE = 60;

	private final String PATH;
	private final int threadSleepMilli = 10;
	/**
	 * state flags
	 */
	private boolean playCompleted;
	private boolean isStopped;
	private boolean isPaused;
	private boolean isPlaying;

	private AudioInputStream audioStream;
	private Clip audioClip;
	private Map<String, String> info;
	private double [][] buffer; 	//buffer arrays for each channel
	private int position;		//current position
	private int channelNumber;
	private int bitDepth;
	private int samplingRate;
	private double min = Double.MAX_VALUE;
	private double max = Double.MIN_VALUE;

	public FilePlayer(String FilePath){
		PATH = FilePath;
	}

	/**
	 * Load audio file
	 * 
	 * @param  FilePath
	 * @throws IOException
	 * @throws UnsupportedAudioFileException
	 * @throws LineUnavailableException
	 */
	public void load() throws UnsupportedAudioFileException, IOException, LineUnavailableException {
		File file = new File(PATH);
		if(file.isFile()) {
			AudioFileFormat aff = AudioSystem.getAudioFileFormat(file);;
			this.info = new TreeMap<>();
			audioStream = AudioSystem.getAudioInputStream(file);
			AudioFormat format = audioStream.getFormat();	
			DataLine.Info infoLine = new DataLine.Info(Clip.class, format);
			audioClip =  (Clip) AudioSystem.getLine(infoLine);
			audioClip.addLineListener(this);
			audioClip.open(audioStream);

			getAudioBuffer(file);
	
			this.info.put("File Name:		", file.getName());
			this.info.put("Channel Number:	", String.valueOf(format.getChannels()));
			this.info.put("Encoding:		", audioStream.getFormat().getEncoding().toString());
			this.info.put("Frame Rate:		", String.valueOf(format.getFrameRate())+" Hz");
			this.info.put("Duration:		", String.valueOf(getClipSecondLength())+" s");
			this.info.put("Sampling Size:	", String.valueOf(format.getSampleSizeInBits())+" Bits");
			this.info.put("Type:		 ", aff.getType().toString());
			this.info.put("File Length:		", String.valueOf(aff.getByteLength()/1000_000.0)+" MByte");

		}
	}

	private void getAudioBuffer(File audioFile) {
		byte[] samples;
		AudioInputStream is;
		DataInputStream dis = null;
		try {
			AudioFormat.Encoding encode = audioStream.getFormat().getEncoding();//AudioFormat.Encoding.PCM_SIGNED
			AudioInputStream audioStream = AudioSystem.getAudioInputStream(audioFile);
			is = AudioSystem.getAudioInputStream(encode, audioStream);
			dis = new DataInputStream(audioStream);      //for using readFully()
			AudioFormat format = is.getFormat();
			samples = new byte[(int)(is.getFrameLength() * format.getFrameSize())];
			dis.readFully(samples);
			this.channelNumber = format.getChannels();
			this.bitDepth = format.getSampleSizeInBits();
			this.buffer = new double[this.channelNumber][samples.length/format.getFrameSize()];
			this.samplingRate = (int) format.getFrameRate();


			WavFile wavFile = WavFile.openWavFile(audioFile);
			// Create a buffer of 100 frames
			double[] buffer = new double[(int) (100 * format.getChannels())];

			int framesRead;
			int runInd = 0;
			do {
				// Read frames into buffer
				framesRead = wavFile.readFrames(buffer, format.getFrameSize());
				// Loop through frames and look for minimum and maximum value
				for (int s=0 ; s < framesRead ; s++) {
					for(int chan = 0; chan < format.getChannels(); chan++ ) {
						this.buffer[chan][runInd] = buffer[s+chan];
						if (buffer[s+chan] > max) max = buffer[s+chan];
						if (buffer[s+chan] < min) min = buffer[s+chan];
					}
					runInd++;
				}
			} while (framesRead != 0);
			wavFile.close();

			setChanged();
			notifyObservers("loadedAudioBuffer");	
		} catch (IOException e) {
			e.printStackTrace();
		} catch (UnsupportedAudioFileException e) {
			e.printStackTrace();
		} catch (Exception e) {
			System.err.println(e);
		} finally {
			try {
				dis.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}


	public Clip createClipFromAudioStreamDefault(AudioInputStream audioStream) throws LineUnavailableException {
		AudioFormat format = audioStream.getFormat();	
		DataLine.Info infoLine = new DataLine.Info(Clip.class, format);
		return (Clip) AudioSystem.getLine(infoLine);
	}

	public void initClip()  throws IOException, LineUnavailableException {
		audioClip.addLineListener(this);
		audioClip.open(audioStream);
	}

	public void closeClip() {
		audioClip.close();
	}

	public void resetClip() {
		this.setPlaying(false);
		this.setPlayCompleted(true);
		setChanged();
		notifyObservers("resetClip");	
		audioClip.setMicrosecondPosition(0);
	}

	public long getClipSecondLength() {
		return audioClip.getMicrosecondLength()/1_000_000; //to seconds
	}

	public String getClipLengthFormatedString() {
		String length = "";
		long hour = 0;
		long minute = 0;
		long seconds = this.getClipSecondLength();

		//seconds in hours
		if (seconds >= SECONDS_IN_HOUR) {
			hour = seconds / SECONDS_IN_HOUR;
			length = String.format("%02d:", hour);
		} else {
			length += "00:";
		}

		//seconds in minutes
		minute = seconds - (hour * SECONDS_IN_HOUR);
		if (minute >= SECONDS_IN_MINUTE) {
			minute = minute / SECONDS_IN_MINUTE;
			length += String.format("%02d:", minute);

		} else {
			minute = 0;
			length += "00:";
		}

		//the rest of seconds
		long second = seconds - hour * SECONDS_IN_HOUR - minute * SECONDS_IN_MINUTE;

		length += String.format("%02d", second);

		return length;
	}

	/**
	 * in order to control the player interupt of Thread.sleep() is used
	 * @throws IOException
	 * 
	 */
	void play() throws IOException {
		this.getAudioClip().start();
		this.setPlayCompleted(false);
		this.setStopped(false);
		this.setPlaying(true);

		while (!this.isPlayCompleted()) {
			synchronized(FilePlayer.class){
				this.position = this.getAudioClip().getFramePosition();
			}

			try {
				Thread.sleep(threadSleepMilli);
			} catch (InterruptedException ex) {
				if (this.isStopped()) {
					audioClip.stop();
					resetClip();
					this.setPlaying(false);
					break;
				}
				if (this.isPaused()) {
					audioClip.stop();
				} else {
					this.getAudioClip().start();//resume
				}
			}
			if(!this.getAudioClip().isRunning() && !this.isPaused()) {
				break;
			}
		}
		resetClip();
	}

	public void stop() {
		isStopped = true;
	}

	public void pause() {
		isPaused = true;
	}

	public void resume() {
		isPaused = false;
	}

	public void resetFlags() {
		this.playCompleted = false;
		this.isStopped = false;
		this.resume();
	}

	/**
	 * Listens to the audio line events knowing when the playback completes.
	 */
	@Override
	public void update(LineEvent event) {
		if (event.getType() == LineEvent.Type.STOP) {
			if (this.isStopped() || !this.isPaused()) {
				this.setPlayCompleted(true);
				setChanged();
				notifyObservers("playIsCompleted");	
			}
		}
	}

	public void showInfo() {
		for (Map.Entry<String, String>  entry : this.info.entrySet()) 
			System.out.println(  "[" + entry.getKey()  + ", " + entry.getValue() + "]"); 
	}



	public Clip getAudioClip() {
		return audioClip;
	}

	public boolean isPlayCompleted() {
		return playCompleted;
	}

	public void setPlayCompleted(boolean playCompleted) {
		this.playCompleted = playCompleted;
	}

	public boolean isStopped() {
		return isStopped;
	}

	public void setStopped(boolean isStopped) {
		this.isStopped = isStopped;
	}

	public boolean isPaused() {
		return isPaused;
	}

	public void setPaused(boolean isPaused) {
		this.isPaused = isPaused;
	}

	public void setAudioClip(Clip audioClip) {
		this.audioClip = audioClip;
	}

	public String getPATH() {
		return PATH;
	}

	public Map<String, String> getInfo() {
		return info;
	}

	public double [][] getBuffer() {
		return buffer;
	}

	public int getPosition() {
		return position;
	}

	public int getThreadSleepMilli() {
		return threadSleepMilli;
	}

	public int getBitDepth() {
		return bitDepth;
	}

	public void setBitDepth(int bitDepth) {
		this.bitDepth = bitDepth;
	}

	public boolean isPlaying() {
		return isPlaying;
	}

	public void setPlaying(boolean isPlaying) {
		this.isPlaying = isPlaying;
	}

	public int getSamplingRate() {
		return samplingRate;
	}

	public void setSamplingRate(int samplingRate) {
		this.samplingRate = samplingRate;
	}

	public double getMin() {
		return min;
	}

	public void setMin(double min) {
		this.min = min;
	}

	public double getMax() {
		return max;
	}

	public void setMax(double max) {
		this.max = max;
	}	
}
