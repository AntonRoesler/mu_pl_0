package main.java.model;

import java.util.LinkedList;
import java.util.Queue;

import main.java.view.GraphPanel;

public class TimeSeries  implements Runnable {
	private double timeWindowSeconds = 0.03; 
	private int size;
	private Queue<Double> scores;
	private FilePlayer filePlayer;
	private Player player;
	private GraphPanel panel;
	private int updateTimeStepMillisec;
	
	public TimeSeries(FilePlayer filePlayer, GraphPanel panel, Player player){
		this.filePlayer = filePlayer;
		this.panel = panel;
		this.player = player;
		this.size = (int) (this.player.getFilePlay().getSamplingRate() * timeWindowSeconds);
		this.updateTimeStepMillisec = (int) (this.timeWindowSeconds*1000/10);
	}
	
	@Override
	public void run() {
		while(!this.filePlayer.isStopped()) {
			try {
				Thread.sleep(this.updateTimeStepMillisec);
			} catch (InterruptedException e) {
				//e.printStackTrace();
				break;
			}
			if(this.player.getTimer()!=null) {
				queryScores(getPosition(), this.filePlayer.getBuffer().length);
			}
			updateGraphPanel();
		}
	}

	private void queryScores(int position, int channelNumber) {
		synchronized (FilePlayer.class) {
			this.scores = new LinkedList<Double>();
			double valAtPosit = 0;
			if(this.size < position)
			for(int i = 0; i < this.size; i++) {
				valAtPosit = 0;
				for(int j = 0; j < channelNumber; j++) {
					valAtPosit += this.filePlayer.getBuffer()[j][position - this.size + i - 1];
				}	
				valAtPosit /= channelNumber;
				valAtPosit /= (this.filePlayer.getMax()-this.filePlayer.getMin());
				valAtPosit += 0.5;
				this.scores.add(valAtPosit);	
			}
		}
	}
	
	private int getPosition() {
		synchronized (FilePlayer.class) {
			int pos = (int) this.player.getTimer().getTimeMillisec()*this.player.getFilePlay().getSamplingRate()/1000;
			if(pos > this.filePlayer.getBuffer()[0].length-1)
				pos = this.filePlayer.getBuffer()[0].length-1;
			return pos;
		}
	}
	
	private void updateGraphPanel() {
		synchronized (TimeSeries.class) {
			if(this.scores!=null && this.scores.size()==this.size) {
				if(!this.filePlayer.isPaused() && this.filePlayer.isPlaying()) {
					panel.setScores(this.scores);
					panel.repaint();
				} else if(this.filePlayer.isStopped() || this.filePlayer.isPlayCompleted()){
					panel.setScores(null);
					panel.reset();
					panel.repaint();
				}
			}
		}
	}
	
	public Queue<Double> getScores() {
		return scores;
	}

	public void setScores(Queue<Double> scores) {
			this.scores = scores;
	}

	public FilePlayer getFilePlayer() {
		return filePlayer;
	}

	public void setFilePlayer(FilePlayer filePlayer) {
		this.filePlayer = filePlayer;
	}

	public GraphPanel getPanel() {
		return panel;
	}

	public void setPanel(GraphPanel panel) {
		this.panel = panel;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public double getTimeWindowSeconds() {
		return timeWindowSeconds;
	}

	public void setTimeWindowSeconds(double timeWindowSeconds) {
		this.timeWindowSeconds = timeWindowSeconds;
	}

	
}
