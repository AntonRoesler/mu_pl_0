package main.java.model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import javax.sound.sampled.Clip;
import javax.swing.JLabel;

/**
 * Measuring the passing time during playing Audio Clip.
 * This is used to update the Timer Label
 *
 */
public class PlayTimerWorker extends Thread {
	private final int timeStepSize = 25;
	private DateFormat dateFormater = new SimpleDateFormat("HH:mm:ss");	
	private boolean isRunning = false;
	private boolean isPause = false;
	private boolean isReset = false;
	private long startTime;
	private long pauseTime;
	
	private JLabel labelTime;
	private Clip audioClip;
	
	public void setAudioClip(Clip audioClip) {
		this.audioClip = audioClip;
	}

	PlayTimerWorker(JLabel labelRecordTime) {
		this.labelTime = labelRecordTime;
	}
	
	public void run() {
		isRunning = true;
		startTime = System.currentTimeMillis();
		while (isRunning) {
			try {
				Thread.sleep(timeStepSize);
				if (!isPause) {
					if (audioClip != null && audioClip.isRunning()) {
						labelTime.setText(toTimeString());
					}
				} else {
					pauseTime += timeStepSize;//update every 100 ms
				}
			} catch (InterruptedException ex) {
//				ex.printStackTrace();
				if (isReset) {
					labelTime.setText("00:00:00");
					isRunning = false;		
					break;
				}
			}
		}
	}

	void reset() {
		isReset = true;
		isRunning = false;
	}
	
	void pauseTimer() {
		isPause = true;
	}
	
	void resumeTimer() {
		isPause = false;
	}
	
	public long getTimeMillisec() {
		synchronized(PlayTimerWorker.class) {
			return System.currentTimeMillis() - startTime - pauseTime;
		}
	} 
	
	public String toTimeString() {
		long now = System.currentTimeMillis();
		Date current = new Date(getTimeMillisec());
		dateFormater.setTimeZone(TimeZone.getTimeZone("GMT"));
		String timeCounter = dateFormater.format(current);
		return timeCounter;
	}
}