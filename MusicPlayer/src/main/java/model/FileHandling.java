package main.java.model;

import java.awt.Component;
import java.io.File;
import java.util.Observable;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;


@SuppressWarnings("deprecation") //Observable deprecated since Java 9
public class FileHandling extends Observable{

	private String filePath;
	private String lastOpenPath;
	
	public FileHandling(){
		setLastOpenPath("src/main/resources/samples");
	}
	
	public void openFile(Component cmp) {
		JFileChooser fileChooser = null;
		
		if (lastOpenPath != null && !lastOpenPath.equals("")) {
			fileChooser = new JFileChooser(lastOpenPath);
		} else {
			fileChooser = new JFileChooser();
		}
		
		FileFilter wavFilter = new FileFilter() {

			@Override
			public String getDescription() {
				return "Sound file (*.WAV)";
			}

			@Override
			public boolean accept(File file) {
				if (file.isDirectory()) {
					return true;
				} else {
					return file.getName().toLowerCase().endsWith(".wav");
				}
			}
		};
		
		fileChooser.setFileFilter(wavFilter);
		fileChooser.setDialogTitle("Open Audio File");
		fileChooser.setAcceptAllFileFilterUsed(false);

		int userChoice = fileChooser.showOpenDialog(cmp);
		if (userChoice == JFileChooser.APPROVE_OPTION) {
			setFilePath(fileChooser.getSelectedFile().getAbsolutePath());
			setLastOpenPath(fileChooser.getSelectedFile().getParent());
		
			setChanged();
			notifyObservers(getFilePath());	
		}
		
		
	}

	
	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getLastOpenPath() {
		return lastOpenPath;
	}

	public void setLastOpenPath(String lastOpenPath) {
		this.lastOpenPath = lastOpenPath;
	}
	
}
